import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const GuardedRoutesAuth = ({ ...props }) => {
    if (props.authenticated) {
        if (props.isVerified) {
            return (
                <Route
                    path={'/'}
                    render={() => <Redirect to={'/dashboard'} />}
                />
            );
        }
        return (
            <Route
                path={'/'}
                render={() => <Redirect to={'/verification-info'} />}
            />
        );
    }
    return (
        <Route
            exact={true}
            path={props.path}
            component={props.component}
        />
    );
}

export default GuardedRoutesAuth;