import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const GuardedRoutesGeneral = ({ ...props }) => {
    if (!props.authenticated) {
        return (
            <Route
                path={'/'}
                render={() => <Redirect to={'/sign-in'} />}
            />
        );
    }

    if (!props.isVerified) {
        return (
            <Route
                path={'/'}
                render={() => <Redirect to={'/verification-info'} />}
            />
        );
    }

    return (
        <Route
            exact={true}
            path={props.path}
            component={props.component}
        />
    );
}

export default GuardedRoutesGeneral;