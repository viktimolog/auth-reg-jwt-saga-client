import React from 'react';
import { Modal } from 'antd';

class EmailVerification extends React.Component {

    handleError = () => this.props.resetError();

    render() {
        return (
            <Modal
                title='This is an error message:'
                visible={!!this.props.isError}
                onOk={this.handleError}
                onCancel={this.handleError}
            >
                <p>{this.props.isError}</p>
            </Modal>
        );
    }
}

export default EmailVerification;