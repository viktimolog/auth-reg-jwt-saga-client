import React from 'react';
import { Button, Modal } from 'antd';
import 'index.css';

class VerificationInfo extends React.Component {

    handleSubmit = () => this.props.resendLetter(this.props.userId);
    handleError = () => this.props.resetError();
    handleMessage = () => this.props.resetMessage();

    render() {
        return (
            <div className='log' >
                <Modal
                    title='This is an error message:'
                    visible={!!this.props.isError}
                    onOk={this.handleError}
                    onCancel={this.handleError}
                >
                    <p>{this.props.isError}</p>
                </Modal>
                <Modal
                    title='This is an information message:'
                    visible={!!this.props.isMessage}
                    onOk={this.handleMessage}
                    onCancel={this.handleMessage}
                >
                    <p>{this.props.isMessage}</p>
                </Modal>
                <div className='text'>Welcome {this.props.email}.</div>
                <div className='text'>You have not passed the email verification.</div>
                <div className='text'>Please, read the last letter from us.</div>
                <div className='text'>Or press Resend letter for the new letter.</div>
                <Button
                    type='primary'
                    htmlType='submit'
                    className='login-form-button'
                    onClick={this.handleSubmit}
                >
                    Resend letter
                </Button>
            </div>
        );
    }
}

export default VerificationInfo;

