import React from 'react';
import { Button } from 'antd';
import 'index.css';

const Dashboard = ({ email, signOut }) => {
    const handleSubmit = () => signOut();
    return (
        <div className='log'>
            <div className='text'>Welcome {email}.</div>
            <div className='text'>Press Exit for sign out.</div>
            <Button
                type='primary'
                htmlType='submit'
                className='login-form-button'
                onClick={handleSubmit}
            >
                Exit
            </Button>
        </div>
    );
}

export default Dashboard;

