import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    resendLetter,
    resetMessage,
    resetError
} from 'store/auth/actions';

import VerificationInfo from 'components/views/VerificationInfo';
import Loading from 'components/views/Loading';

class VerificationInfoPage extends React.Component {
    static propTypes = {
        isMessage: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        resendLetter: PropTypes.func.isRequired,
        resetMessage: PropTypes.func.isRequired,
        isLoading: PropTypes.bool.isRequired,
        email: PropTypes.string,
        userId: PropTypes.string
    };

    render() {
        const { resetError, isError, isMessage, email, userId, resendLetter, resetMessage, isLoading } = this.props;
        if (isLoading) return <Loading />
        return (
            <VerificationInfo
                email={email}
                userId={userId}
                isMessage={isMessage}
                isError={isError}
                resendLetter={resendLetter}
                resetMessage={resetMessage}
                resetError={resetError}
            />
        );
    }
}

const mapStateToProps = state => ({
    email: state.authReducer.email,
    userId: state.authReducer.userId,
    isMessage: state.authReducer.isMessage,
    isError: state.authReducer.isError,
    isLoading: state.authReducer.isLoading
});

const mapDispatchToProps = dispatch => ({
    resendLetter: id => dispatch(resendLetter(id)),
    resetMessage: () => dispatch(resetMessage()),
    resetError: () => dispatch(resetError())
})

export default connect(mapStateToProps, mapDispatchToProps)(VerificationInfoPage);