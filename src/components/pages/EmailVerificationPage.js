import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { verify, resetError, authenticate } from 'store/auth/actions';

import EmailVerification from 'components/views/EmailVerification';
import Loading from 'components/views/Loading';

class EmailVerificationPage extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        verify: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.authenticate();
        this.props.verify({ verificationKey: this.props.match.params.verificationKey });
    }

    render() {
        const { isError, isLoading, resetError } = this.props;
        if (isLoading) return <Loading />
        return (
            <EmailVerification
                isError={isError}
                resetError={resetError}
            />
        );
    }
}

const mapStateToProps = state => ({
    isError: state.authReducer.isError,
    isLoading: state.authReducer.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators({ verify, resetError, authenticate }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EmailVerificationPage);