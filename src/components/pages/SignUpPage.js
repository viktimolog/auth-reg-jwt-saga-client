import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { signUp, resetError } from 'store/auth/actions';

import SignUp from 'components/forms/SignUp';
import Loading from 'components/views/Loading';

class SignUpPage extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        signUp: PropTypes.func.isRequired
    };

    render() {
        const { signUp, isError, resetError, isLoading } = this.props;
        if (isLoading) return <Loading />
        return (
            <SignUp
                signUp={signUp}
                isError={isError}
                resetError={resetError}
                isLoading={isLoading}
            />
        );
    }
}

const mapStateToProps = state => ({
    isError: state.authReducer.isError,
    isLoading: state.authReducer.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators({ signUp, resetError }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage);