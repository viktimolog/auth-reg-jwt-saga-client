import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { signIn, resetError } from 'store/auth/actions';

import SignIn from 'components/forms/SignIn';

class SignInPage extends React.Component {
    static propTypes = {
        isError: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ]).isRequired,
        signIn: PropTypes.func.isRequired
    };
    
    render() {
        const { signIn, isError, resetError, isLoading } = this.props;
        return (
            <SignIn
                signIn={signIn}
                isError={isError}
                resetError={resetError}
                isLoading={isLoading}
            />
        );
    }
}

const mapStateToProps = state => ({
    isError: state.authReducer.isError,
    isLoading: state.authReducer.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators({ signIn, resetError }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage);