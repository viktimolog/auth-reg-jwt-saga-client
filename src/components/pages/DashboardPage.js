import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    signOut
} from 'store/auth/actions';

import Dashboard from 'components/views/Dashboard';

class DashboardPage extends React.Component {
    static propTypes = {
        signOut: PropTypes.func.isRequired,
        email: PropTypes.string
    };
    render() {
        return (
            <Dashboard
                signOut={this.props.signOut}
                email={this.props.email}
            />
        );
    }
}

const mapStateToProps = state => ({
    email: state.authReducer.email
});

const mapDispatchToProps = dispatch => ({
    signOut: () => dispatch(signOut())
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);