import React from 'react';
import { Form, Icon, Input, Button, Modal } from 'antd';
import { Link } from 'react-router-dom';
import 'index.css';

import Loading from 'components/views/Loading';

const FormItem = Form.Item;

class SignUp extends React.Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                if (values.password.trim() !== values.password2.trim()) {
                    return Modal.error({
                        title: 'This is an error message:',
                        content: 'Passwords do not match!'
                    });
                }
                const credentials = {
                    email: values.email.trim(),
                    password: values.password.trim()
                };
                this.props.signUp(credentials);
            }
        });
    };

    handleError = () => this.props.resetError();

    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <div>
                {
                    this.props.isLoading
                        ? <Loading />
                        : null
                }
                <div className='log'>
                    <Modal
                        title='This is an error message:'
                        visible={!!this.props.isError}
                        onOk={this.handleError}
                        onCancel={this.handleError}
                    >
                        <p>{this.props.isError}</p>
                    </Modal>
                    <Form onSubmit={this.handleSubmit} className='login-form'>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please input your email!' }],
                            })(
                                <Input prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Email' />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} type='password' placeholder='Password' />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password2', {
                                rules: [{ required: true, message: 'Please repeat your Password!' }],
                            })(
                                <Input prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} type='password' placeholder='Repeat password' />
                            )}
                        </FormItem>
                        <FormItem>
                            <Button type='primary' htmlType='submit' className='login-form-button'>
                                Sign Up
                        </Button>
                            Or <Link to='/sign-in'>  already have an account? sign-in</Link>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }
}

const SignUpForm = Form.create()(SignUp);

export default SignUpForm;

