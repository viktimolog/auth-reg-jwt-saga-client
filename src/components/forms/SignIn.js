import React from 'react';
import { Form, Icon, Input, Button, Modal } from 'antd';
import { Link } from 'react-router-dom';
import 'index.css';

import Loading from 'components/views/Loading';

const FormItem = Form.Item;

class SignIn extends React.Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const credentials = {
                    email: values.email.trim(),
                    password: values.password.trim()
                };
                this.props.signIn(credentials);
            }
        });
    };

    handleError = () => this.props.resetError();

    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <div>
                {
                    this.props.isLoading
                        ? <Loading />
                        : null
                }
                <div className='log'>
                    <Modal
                        title='This is an error message:'
                        visible={!!this.props.isError}
                        onOk={this.handleError}
                        onCancel={this.handleError}
                    >
                        <p>{this.props.isError}</p>
                    </Modal>
                    <Form onSubmit={this.handleSubmit} className='login-form'>
                        <FormItem>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please input your email!' }],
                            })(
                                <Input prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder='Email' />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} type='password' placeholder='Password' />
                            )}
                        </FormItem>
                        <FormItem>
                            <Button type='primary' htmlType='submit' className='login-form-button'>
                                Log in
                        </Button>
                            Or <Link to='/sign-up'>  register now!</Link>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }
}

const SignInForm = Form.create()(SignIn);

export default SignInForm;

