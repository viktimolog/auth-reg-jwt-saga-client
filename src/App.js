import React from 'react';
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import { BrowserRouter, Switch } from 'react-router-dom';

import { authenticate } from 'store/auth/actions';

import SignUpPage from 'components/pages/SignUpPage';
import SignInPage from 'components/pages/SignInPage';
import DashboardPage from 'components/pages/DashboardPage';
import EmailVerificationPage from 'components/pages/EmailVerificationPage';
import VerificationInfoPage from 'components/pages/VerificationInfoPage';

import GuardedRoutesAuth from 'components/routes/GuardedRoutesAuth';
import GuardedRoutesGeneral from 'components/routes/GuardedRoutesGeneral';
import GuardedRoutesVerification from 'components/routes/GuardedRoutesVerification';

class App extends React.Component {
    static propTypes = {
        authenticated: PropTypes.bool.isRequired,
        isVerified: PropTypes.bool,
        authenticate: PropTypes.func.isRequired,
    };

    componentDidMount() {
        this.props.authenticate();
    }

    render() {
        const { authenticated, isVerified } = this.props;
        return (
            <BrowserRouter>
                <Switch>
                    <GuardedRoutesAuth
                        exact={true}
                        path={'/sign-in'}
                        component={SignInPage}
                        authenticated={authenticated}
                        isVerified={isVerified}
                    />

                    <GuardedRoutesAuth
                        exact={true}
                        path={'/sign-up'}
                        component={SignUpPage}
                        authenticated={authenticated}
                        isVerified={isVerified}
                    />

                    <GuardedRoutesGeneral
                        exact={true}
                        path={'/'}
                        component={DashboardPage}
                        authenticated={authenticated}
                        isVerified={isVerified}
                    />

                    <GuardedRoutesGeneral
                        exact={true}
                        path={'/dashboard'}
                        component={DashboardPage}
                        authenticated={authenticated}
                        isVerified={isVerified}
                    />
                    <GuardedRoutesVerification
                        exact={true}
                        path={'/verification-info'}
                        component={VerificationInfoPage}
                        authenticated={authenticated}
                        isVerified={isVerified}
                    />
                    <GuardedRoutesVerification
                        path={'/email-verification/:verificationKey'}
                        component={EmailVerificationPage}
                        authenticated={authenticated}
                        isVerified={isVerified}
                    />
                </Switch>
            </BrowserRouter>
        );
    }
}

const mapStateToProps = state => ({
    authenticated: state.authReducer.authenticated,
    isVerified: state.authReducer.isVerified
});

const mapDispatchToProps = dispatch => ({
    authenticate: () => dispatch(authenticate())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);



