const baseApiUrl = 'http://localhost:7777/'

const urls = {
    baseApiUrl: baseApiUrl,
    signin: baseApiUrl + 'users/login',
    signup: baseApiUrl + 'users/register',
    verify: baseApiUrl + 'users/verify',
    account: baseApiUrl + 'users/account',
    resendLetter: baseApiUrl + 'users/resendLetter'
};

export default urls;
