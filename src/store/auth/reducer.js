import {
    AUTH_CHECK,
    SIGN_IN,
    SIGN_OUT,
    SIGN_UP,
    ERROR_TRUE,
    ERROR_FALSE,
    MESSAGE_TRUE,
    MESSAGE_FALSE,
    VERIFICATION_TRUE,
    SIGN_IN_LOADER,
    SIGN_UP_LOADER,
    VERIFY_LOADER,
    AUTH_LOADER,
    RESEND_LETTER_LOADER
} from './action-types';

const initialState = {
    authenticated: !!localStorage.getItem('token') || false,
    userId: undefined,
    email: undefined,
    isVerified: false,
    isError: false,
    isMessage: false,
    isLoading: false
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {

        case RESEND_LETTER_LOADER:
        case AUTH_LOADER:
        case VERIFY_LOADER:
        case SIGN_IN_LOADER:
        case SIGN_UP_LOADER: {
            return {
                ...state,
                isLoading: true
            };
        }

        case AUTH_CHECK: {
            return {
                ...state,
                authenticated: !!action.payload.userId || false,
                userId: action.payload.userId || undefined,
                email: action.payload.email || undefined,
                isVerified: action.payload.isVerified || false,
                isLoading: false
            };
        }

        case SIGN_OUT: {
            return {
                ...state,
                authenticated: false,
                userId: undefined,
                email: undefined,
                isVerified: false,
                isError: false,
                isMessage: false
            };
        }

        case SIGN_UP: {
            return {
                ...state,
                userId: action.payload.account.userId,
                email: action.payload.account.email,
                // isVerified: action.payload.account.isVerified,
                isVerified: true,
                authenticated: true,
                isLoading: false
            };
        }

        case VERIFICATION_TRUE: {
            return {
                ...state,
                isVerified: true,
                isLoading: false
            };
        }

        case MESSAGE_TRUE: {
            return {
                ...state,
                isMessage: action.payload.message,
                isLoading: false
            };
        }

        case MESSAGE_FALSE: {
            return {
                ...state,
                isMessage: false
            };
        }

        case SIGN_IN: {
            return {
                ...state,
                userId: action.payload.account.userId,
                email: action.payload.account.email,
                // isVerified: action.payload.account.isVerified,
                isVerified: true,
                authenticated: true,
                isLoading: false
            };
        }

        case ERROR_TRUE: {
            return {
                ...state,
                isError: action.payload.errors[0].message,
                isLoading: false
            };
        }

        case ERROR_FALSE: {
            return {
                ...state,
                isError: false,
            };
        }

        default:
            return state;
    }
};

export default authReducer;