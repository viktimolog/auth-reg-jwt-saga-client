import urls from 'constants/urls';
import axios from 'axios';

export const fetchSignInApi = action => {
    const credentials = {
        email: action.payload.email,
        password: action.payload.password
    };
    return axios.post(urls.signin, credentials);
};

export const fetchSignUpApi = action => {
    const credentials = {
        email: action.payload.email,
        password: action.payload.password
    };
    return axios.post(urls.signup, credentials);
};

export const fetchVerifyApi = action => axios.post(urls.verify, { verificationKey: action.payload.verificationKey });

export const fetchAuthApi = () => axios.post(urls.account);

export const fetchResendLetterApi = () => axios.post(urls.resendLetter);

