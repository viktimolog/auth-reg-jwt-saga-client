import {
    SIGN_OUT,
    MESSAGE_FALSE,
    ERROR_FALSE,
    SIGN_IN_LOADER,
    SIGN_UP_LOADER,
    VERIFY_LOADER,
    AUTH_LOADER,
    RESEND_LETTER_LOADER
} from './action-types';

export const signIn = credentials => ({
    type: SIGN_IN_LOADER,
    payload: credentials
});

export const signUp = credentials => ({
    type: SIGN_UP_LOADER,
    payload: credentials
});

export const verify = verificationKey => ({
    type: VERIFY_LOADER,
    payload: verificationKey
});

export const authenticate = () => ({
    type: AUTH_LOADER
});

export const resendLetter = () => ({
    type: RESEND_LETTER_LOADER
});

export const resetError = () => ({ type: ERROR_FALSE });

export const resetMessage = () => ({ type: MESSAGE_FALSE });

export const signOut = () => {
    localStorage.clear();
    return { type: SIGN_OUT }
};
