import { call, put, all, takeLatest } from 'redux-saga/effects'
import axios from 'axios';
import {
    AUTH_CHECK,
    SIGN_IN,
    SIGN_UP,
    ERROR_TRUE,
    MESSAGE_TRUE,
    VERIFICATION_TRUE,
    SIGN_IN_LOADER,
    SIGN_UP_LOADER,
    VERIFY_LOADER,
    AUTH_LOADER,
    RESEND_LETTER_LOADER
} from './action-types';

import { fetchResendLetterApi, fetchAuthApi, fetchVerifyApi, fetchSignInApi, fetchSignUpApi } from './axios-requests';

const saveToken = token => {
    localStorage.setItem('token', token);
    axios.defaults.headers = {
        Authorization: `Bearer ${token}`
    };
};

function* fetchSignIn(action) {
    try {
        const res = yield call(fetchSignInApi, action);

        if (!res.data.success) {
            return yield put({
                type: ERROR_TRUE,
                payload: res.data.payload
            });
        }

        saveToken(res.data.payload.token);

        yield put({ type: SIGN_IN, payload: res.data.payload });

    } catch (e) {
        yield put({ type: ERROR_TRUE, payload: e.message });
    }
}

function* fetchSignUp(action) {
    try {
        const res = yield call(fetchSignUpApi, action);

        if (!res.data.success) {
            return yield put({
                type: ERROR_TRUE,
                payload: res.data.payload
            });
        }

        saveToken(res.data.payload.token);

        yield put({ type: SIGN_UP, payload: res.data.payload });

    } catch (e) {
        yield put({ type: ERROR_TRUE, payload: e.message });
    }
}

function* fetchVerify(action) {
    try {
        const res = yield call(fetchVerifyApi, action);

        if (!res.data.success) {
            return yield put({
                type: ERROR_TRUE,
                payload: res.data.payload
            });
        }

        yield put({
            type: VERIFICATION_TRUE,
            payload: res.data.payload
        });

    } catch (e) {
        yield put({ type: ERROR_TRUE, payload: e.message });
    }
}

function* fetchAuthenticate(action) {
    try {
        const token = localStorage.getItem('token') || false;
        if (token) {
            axios.defaults.headers = {
                Authorization: `Bearer ${token}`
            };
            const res = yield call(fetchAuthApi, action);

            if (!res.data.success) {
                return yield put({
                    type: ERROR_TRUE,
                    payload: res.data.payload
                });
            }

            return yield put({
                type: AUTH_CHECK,
                payload: res.data.payload
            });
        }
        yield put({
            type: AUTH_CHECK,
            payload: {
                authenticated: false
            }
        });

    } catch (e) {
        yield put({ type: ERROR_TRUE, payload: e.message });
    }
}

function* fetchResendLetter(action) {
    try {
        const res = yield call(fetchResendLetterApi, action);

        if (!res.data.success) {
            return yield put({
                type: ERROR_TRUE,
                payload: res.data.payload
            });
        }

        yield put({
            type: MESSAGE_TRUE,
            payload: res.data.payload
        });

    } catch (e) {
        yield put({ type: ERROR_TRUE, payload: e.message });
    }
}

function* signInWatcher() {
    yield takeLatest(SIGN_IN_LOADER, fetchSignIn);
}

function* signUpWatcher() {
    yield takeLatest(SIGN_UP_LOADER, fetchSignUp);
}

function* verifyWatcher() {
    yield takeLatest(VERIFY_LOADER, fetchVerify);
}

function* authWatcher() {
    yield takeLatest(AUTH_LOADER, fetchAuthenticate);
}

function* resendLetterWatcher() {
    yield takeLatest(RESEND_LETTER_LOADER, fetchResendLetter);
}

export default function* rootSaga() {
    yield all([
        signInWatcher(),
        signUpWatcher(),
        verifyWatcher(),
        authWatcher(),
        resendLetterWatcher()
    ]);
}

