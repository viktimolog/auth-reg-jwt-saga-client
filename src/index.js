import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import App from './App';
import authReducer from 'store/auth/reducer';

import rootSaga from 'store/auth/sagas';

const rootReducer = combineReducers({
    authReducer
});

const history = createHistory();

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

// mount it on the Store
const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(sagaMiddleware)
    )
);

// then run the saga watchers
sagaMiddleware.run(rootSaga);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            < App />
        </ConnectedRouter>
    </Provider>, document.getElementById('root'));
















